"use strict";

let array = ['hello', 'world', 23, '23', null, 235, 'google', undefined];
let arrTypeOfData = 'string';

function filterBy(array, arrTypeOfData) {
    return array.filter((el) => typeof el !== arrTypeOfData);  
};
console.log(filterBy(array, arrTypeOfData));